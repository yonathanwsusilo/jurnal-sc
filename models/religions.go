package models

import "time"

type Religion struct {
	ID        string    `json:id`
	Name      string    `json:name`
	CreatedAt time.Time `json:createdat`
	CreatedBy string    `json:createdby`
	UpdatedAt time.Time `json:updatedat`
	UpdatedBy string    `json:updatedby`
	IsActive  bool      `json:isActive`
}
