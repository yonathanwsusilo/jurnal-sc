package models

import "time"

type Activity struct {
	ID        string    `json:id`
	Name      string    `json:name`
	CreatedAt time.Time `json:createdat`
	CreatedBy string    `json:createdby`
	UpdatedAt time.Time `json:updatedat`
	UpdatedBy string    `json:updatedby`
	IsActive  bool      `json:isActive`
}

type ActivityDetail struct {
	ID         string    `json:id`
	Name       string    `json:name`
	Score      int       `json:score`
	ActivityID string    `json:activityid`
	CreatedAt  time.Time `json:createdat`
	CreatedBy  string    `json:createdby`
	UpdatedAt  time.Time `json:updatedat`
	UpdatedBy  string    `json:updatedby`
	IsActive   bool      `json:isActive`
}
