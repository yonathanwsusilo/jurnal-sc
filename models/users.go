package models

import "time"

type UserRole struct {
	ID        string    `json:id`
	Name      string    `json:name`
	CreatedAt time.Time `json:createdat`
	CreatedBy string    `json:createdby`
	UpdatedAt time.Time `json:updatedat`
	UpdatedBy string    `json:updatedby`
	IsActive  bool      `json:isActive`
}

type User struct {
	ID                 string    `json:id`
	StudentCouncelorID string    `json:studentcouncelorid`
	Email              string    `json:email`
	Password           string    `json:password`
	UserRole           string    `json:userrole`
	IsVerified         bool      `json:isverified`
	CreatedAt          time.Time `json:createdat`
	CreatedBy          string    `json:createdby`
	UpdatedAt          time.Time `json:updatedat`
	UpdatedBy          string    `json:updatedby`
	IsActive           bool      `json:isActive`
}
