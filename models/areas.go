package models

import "time"

type Area struct {
	ID        string    `json:id`
	Name      string    `json:name`
	Code      string    `json:code`
	CreatedAt time.Time `json:createdat`
	CreatedBy string    `json:createdby`
	UpdatedAt time.Time `json:updatedat`
	UpdatedBy string    `json:updatedby`
	IsActive  bool      `json:isActive`
}
