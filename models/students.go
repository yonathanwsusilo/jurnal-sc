package models

import "time"

type Student struct {
	ID               string    `json:id`
	Code             string    `json:code`
	Area             string    `json:area`
	RegistrationDate time.Time `json:registrationdate`
	FullName         string    `json:fullname`
	Nickname         string    `json:nickname`
	Sex              string    `json:sex`
	Address          string    `json:address`
	HomePhone        string    `json:homephone`
	CellPhone        string    `json:cellphone`
	LineID           string    `json:lineid`
	Email            string    `json:email`
	SchoolName       string    `json:schoolname`
	Year             string    `json:year`
	FutureYear       string    `json:futureyear`
	DOB              time.Time `json:dob`
	Age              string    `json:age`
	Religion         string    `json:religion`
	WorshipPlace     string    `json:worshipplace`
	IsBaptized       bool      `json:isbaptized`
	BaptizedYear     int       `json:baptizedyear`
	KsSiDm           string    `json:kssidm`
	CreatedAt        time.Time `json:createdat`
	CreatedBy        string    `json:createdby`
	UpdatedAt        time.Time `json:updatedat`
	UpdatedBy        string    `json:updatedby`
	IsActive         bool      `json:isactive`
}
