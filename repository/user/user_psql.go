package userRepository

import (
	"database/sql"
	"fmt"
	"log"
	"sc-journal/models"
)

type UserRepository struct{}

func logFatalUser(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

//userRoles
func (c UserRepository) GetUserRoles(db *sql.DB, userRole models.UserRole, userRoles []models.UserRole) ([]models.UserRole, error) {
	rows, err := db.Query("select * from lk_user_roles")
	if err != nil {
		return []models.UserRole{}, err
	}
	for rows.Next() {
		err = rows.Scan(&userRole.ID, &userRole.Name, &userRole.CreatedAt, &userRole.CreatedBy, &userRole.UpdatedAt, &userRole.UpdatedBy, &userRole.IsActive)
		fmt.Println(err)
		//fmt.Println("userRoles before", userRoles)
		userRoles = append(userRoles, userRole)
		//fmt.Println("userRoles after ", userRoles)
	}
	if err != nil {
		return []models.UserRole{}, err
	}
	//fmt.Println("returned userRoles ", userRoles)
	return userRoles, nil
}

func (c UserRepository) GetActiveUserRoles(db *sql.DB, userRole models.UserRole, userRoles []models.UserRole) ([]models.UserRole, error) {
	rows, err := db.Query("select * from lk_user_roles where isactive=true")

	if err != nil {
		return []models.UserRole{}, err
	}

	for rows.Next() {
		err = rows.Scan(&userRole.ID, &userRole.Name, &userRole.CreatedAt, &userRole.CreatedBy, &userRole.UpdatedAt, &userRole.UpdatedBy, &userRole.IsActive)
		userRoles = append(userRoles, userRole)
	}

	if err != nil {
		return []models.UserRole{}, err
	}

	return userRoles, nil
}

func (c UserRepository) GetUserRole(db *sql.DB, userRole models.UserRole, id string) (models.UserRole, error) {
	rows := db.QueryRow("select * from lk_user_roles where id=$1", id)
	err := rows.Scan(&userRole.ID, &userRole.Name, &userRole.CreatedAt, &userRole.CreatedBy, &userRole.UpdatedAt, &userRole.UpdatedBy, &userRole.IsActive)

	return userRole, err
}

func (c UserRepository) AddUserRole(db *sql.DB, userRole models.UserRole) (string, error) {
	err := db.QueryRow("insert into lk_user_roles (user_role_name, created_by, updated_by, isActive) values($1, $2, $3, $4) RETURNING id;",
		userRole.Name, userRole.CreatedBy, userRole.UpdatedBy, userRole.IsActive).Scan(&userRole.ID)

	if err != nil {
		return "", err
	}

	userRole.IsActive = true
	return userRole.ID, nil
}

func (c UserRepository) UpdateUserRole(db *sql.DB, userRole models.UserRole) (int64, error) {
	result, err := db.Exec("update lk_user_roles set user_role_name=$1, updated_at=NOW(), updated_by=$2 where id=$3 RETURNING id",
		&userRole.Name, &userRole.UpdatedBy, &userRole.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (c UserRepository) SoftDeleteUserRole(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_user_roles set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//registration
func (c UserRepository) GetUsers(db *sql.DB, user models.User, users []models.User) ([]models.User, error) {
	rows, err := db.Query("select * from ms_users")
	if err != nil {
		return []models.User{}, err
	}
	for rows.Next() {
		err = rows.Scan(&user.ID, &user.StudentCouncelorID, &user.Email, &user.Password, &user.UserRole, &user.IsVerified, &user.CreatedAt, &user.CreatedBy, &user.UpdatedAt, &user.UpdatedBy, &user.IsActive)
		fmt.Println(err)
		//fmt.Println("userRoles before", userRoles)
		users = append(users, user)
		//fmt.Println("userRoles after ", userRoles)
	}
	if err != nil {
		return []models.User{}, err
	}
	//fmt.Println("returned userRoles ", userRoles)
	return users, nil
}
