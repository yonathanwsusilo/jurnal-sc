package jurnalRepository

import (
	"database/sql"
	"log"
	"sc-journal/models"
)

type JurnalRepository struct{}

func logFatalJurnal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

//areas
func (b JurnalRepository) GetAreas(db *sql.DB, area models.Area, areas []models.Area) ([]models.Area, error) {
	rows, err := db.Query("select * from lk_areas")

	if err != nil {
		return []models.Area{}, err
	}

	for rows.Next() {
		err = rows.Scan(&area.ID, &area.Name, &area.Code, &area.CreatedAt, &area.CreatedBy, &area.UpdatedAt, &area.UpdatedBy, &area.IsActive)
		areas = append(areas, area)
	}

	if err != nil {
		return []models.Area{}, err
	}

	return areas, nil
}

func (b JurnalRepository) GetActiveAreas(db *sql.DB, area models.Area, areas []models.Area) ([]models.Area, error) {
	rows, err := db.Query("select * from lk_areas where isactive=true")

	if err != nil {
		return []models.Area{}, err
	}

	for rows.Next() {
		err = rows.Scan(&area.ID, &area.Name, &area.Code, &area.CreatedAt, &area.CreatedBy, &area.UpdatedAt, &area.UpdatedBy, &area.IsActive)
		areas = append(areas, area)
	}

	if err != nil {
		return []models.Area{}, err
	}

	return areas, nil
}

func (b JurnalRepository) GetArea(db *sql.DB, area models.Area, id string) (models.Area, error) {
	rows := db.QueryRow("select * from lk_areas where id=$1", id)
	err := rows.Scan(&area.ID, &area.Name, &area.Code, &area.CreatedAt, &area.CreatedBy, &area.UpdatedAt, &area.UpdatedBy, &area.IsActive)

	return area, err
}

func (b JurnalRepository) AddArea(db *sql.DB, area models.Area) (string, error) {
	err := db.QueryRow("insert into lk_areas (area_name, area_code, created_by, updated_by, isActive) values($1, $2, $3, $4, $5) RETURNING id;",
		area.Name, area.Code, area.CreatedBy, area.UpdatedBy, area.IsActive).Scan(&area.ID)

	if err != nil {
		return "", err
	}

	area.IsActive = true
	return area.ID, nil
}

func (b JurnalRepository) UpdateArea(db *sql.DB, area models.Area) (int64, error) {
	result, err := db.Exec("update lk_areas set area_name=$1, area_code=$2, updated_at=NOW(), updated_by=$3 where id=$4 RETURNING id",
		&area.Name, &area.Code, &area.UpdatedBy, &area.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) RemoveArea(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("delete from lk_areas where id = $1", id)

	if err != nil {
		return 0, err
	}

	rowsDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsDeleted, nil
}

func (b JurnalRepository) SoftDeleteArea(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_areas set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//ksSiDm
func (b JurnalRepository) GetKsSiDms(db *sql.DB, ksSiDm models.KsSiDm, ksSiDms []models.KsSiDm) ([]models.KsSiDm, error) {
	rows, err := db.Query("select * from lk_ks_si_dm")

	if err != nil {
		return []models.KsSiDm{}, err
	}

	for rows.Next() {
		err = rows.Scan(&ksSiDm.ID, &ksSiDm.Name, &ksSiDm.Code, &ksSiDm.CreatedAt, &ksSiDm.CreatedBy, &ksSiDm.UpdatedAt, &ksSiDm.UpdatedBy, &ksSiDm.IsActive)
		ksSiDms = append(ksSiDms, ksSiDm)
	}

	if err != nil {
		return []models.KsSiDm{}, err
	}

	return ksSiDms, nil
}

func (b JurnalRepository) GetActiveKsSiDms(db *sql.DB, ksSiDm models.KsSiDm, ksSiDms []models.KsSiDm) ([]models.KsSiDm, error) {
	rows, err := db.Query("select * from lk_ks_si_dm where isactive=true")

	if err != nil {
		return []models.KsSiDm{}, err
	}

	for rows.Next() {
		err = rows.Scan(&ksSiDm.ID, &ksSiDm.Name, &ksSiDm.Code, &ksSiDm.CreatedAt, &ksSiDm.CreatedBy, &ksSiDm.UpdatedAt, &ksSiDm.UpdatedBy, &ksSiDm.IsActive)
		ksSiDms = append(ksSiDms, ksSiDm)
	}

	if err != nil {
		return []models.KsSiDm{}, err
	}

	return ksSiDms, nil
}

func (b JurnalRepository) GetKsSiDm(db *sql.DB, ksSiDm models.KsSiDm, id string) (models.KsSiDm, error) {
	rows := db.QueryRow("select * from lk_ks_si_dm where id=$1", id)
	err := rows.Scan(&ksSiDm.ID, &ksSiDm.Name, &ksSiDm.Code, &ksSiDm.CreatedAt, &ksSiDm.CreatedBy, &ksSiDm.UpdatedAt, &ksSiDm.UpdatedBy, &ksSiDm.IsActive)

	return ksSiDm, err
}

func (b JurnalRepository) AddKsSiDm(db *sql.DB, ksSiDm models.KsSiDm) (string, error) {
	err := db.QueryRow("insert into lk_ks_si_dm (status_name, status_code, created_by, updated_by, isActive) values($1, $2, $3, $4, $5) RETURNING id;",
		ksSiDm.Name, ksSiDm.Code, ksSiDm.CreatedBy, ksSiDm.UpdatedBy, ksSiDm.IsActive).Scan(&ksSiDm.ID)

	if err != nil {
		return "", err
	}

	ksSiDm.IsActive = true
	return ksSiDm.ID, nil
}

func (b JurnalRepository) UpdateKsSiDm(db *sql.DB, ksSiDm models.KsSiDm) (int64, error) {
	result, err := db.Exec("update lk_ks_si_dm set status_name=$1, status_code=$2, updated_at=NOW(), updated_by=$3 where id=$4 RETURNING id",
		&ksSiDm.Name, &ksSiDm.Code, &ksSiDm.UpdatedBy, &ksSiDm.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteKsSiDm(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_ks_si_dm set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//religion
func (b JurnalRepository) GetReligions(db *sql.DB, religion models.Religion, religions []models.Religion) ([]models.Religion, error) {
	rows, err := db.Query("select * from lk_religions")

	if err != nil {
		return []models.Religion{}, err
	}

	for rows.Next() {
		err = rows.Scan(&religion.ID, &religion.Name, &religion.CreatedAt, &religion.CreatedBy, &religion.UpdatedAt, &religion.UpdatedBy, &religion.IsActive)
		religions = append(religions, religion)
	}

	if err != nil {
		return []models.Religion{}, err
	}

	return religions, nil
}

func (b JurnalRepository) GetActiveReligions(db *sql.DB, religion models.Religion, religions []models.Religion) ([]models.Religion, error) {
	rows, err := db.Query("select * from lk_religions where isactive=true")

	if err != nil {
		return []models.Religion{}, err
	}

	for rows.Next() {
		err = rows.Scan(&religion.ID, &religion.Name, &religion.CreatedAt, &religion.CreatedBy, &religion.UpdatedAt, &religion.UpdatedBy, &religion.IsActive)
		religions = append(religions, religion)
	}

	if err != nil {
		return []models.Religion{}, err
	}

	return religions, nil
}

func (b JurnalRepository) GetReligion(db *sql.DB, religion models.Religion, id string) (models.Religion, error) {
	rows := db.QueryRow("select * from lk_religions where id=$1", id)
	err := rows.Scan(&religion.ID, &religion.Name, &religion.CreatedAt, &religion.CreatedBy, &religion.UpdatedAt, &religion.UpdatedBy, &religion.IsActive)

	return religion, err
}

func (b JurnalRepository) AddReligion(db *sql.DB, religion models.Religion) (string, error) {
	err := db.QueryRow("insert into lk_religions (religion_name, created_by, updated_by, isActive) values($1, $2, $3, $4) RETURNING id;",
		religion.Name, religion.CreatedBy, religion.UpdatedBy, religion.IsActive).Scan(&religion.ID)

	if err != nil {
		return "", err
	}

	religion.IsActive = true
	return religion.ID, nil
}

func (b JurnalRepository) UpdateReligion(db *sql.DB, religion models.Religion) (int64, error) {
	result, err := db.Exec("update lk_religions set religion_name=$1, updated_at=NOW(), updated_by=$2 where id=$3 RETURNING id",
		&religion.Name, &religion.UpdatedBy, &religion.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteReligion(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_religions set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//subject
func (b JurnalRepository) GetSubjects(db *sql.DB, subject models.Subject, subjects []models.Subject) ([]models.Subject, error) {
	rows, err := db.Query("select * from lk_subjects")

	if err != nil {
		return []models.Subject{}, err
	}

	for rows.Next() {
		err = rows.Scan(&subject.ID, &subject.Name, &subject.CreatedAt, &subject.CreatedBy, &subject.UpdatedAt, &subject.UpdatedBy, &subject.IsActive)
		subjects = append(subjects, subject)
	}

	if err != nil {
		return []models.Subject{}, err
	}

	return subjects, nil
}

func (b JurnalRepository) GetActiveSubjects(db *sql.DB, subject models.Subject, subjects []models.Subject) ([]models.Subject, error) {
	rows, err := db.Query("select * from lk_subjects where isactive=true")

	if err != nil {
		return []models.Subject{}, err
	}

	for rows.Next() {
		err = rows.Scan(&subject.ID, &subject.Name, &subject.CreatedAt, &subject.CreatedBy, &subject.UpdatedAt, &subject.UpdatedBy, &subject.IsActive)
		subjects = append(subjects, subject)
	}

	if err != nil {
		return []models.Subject{}, err
	}

	return subjects, nil
}

func (b JurnalRepository) GetSubject(db *sql.DB, subject models.Subject, id string) (models.Subject, error) {
	rows := db.QueryRow("select * from lk_subjects where id=$1", id)
	err := rows.Scan(&subject.ID, &subject.Name, &subject.CreatedAt, &subject.CreatedBy, &subject.UpdatedAt, &subject.UpdatedBy, &subject.IsActive)

	return subject, err
}

func (b JurnalRepository) AddSubject(db *sql.DB, subject models.Subject) (string, error) {
	err := db.QueryRow("insert into lk_subjects (subject_name, created_by, updated_by, isActive) values($1, $2, $3, $4) RETURNING id;",
		subject.Name, subject.CreatedBy, subject.UpdatedBy, subject.IsActive).Scan(&subject.ID)

	if err != nil {
		return "", err
	}

	subject.IsActive = true
	return subject.ID, nil
}

func (b JurnalRepository) UpdateSubject(db *sql.DB, subject models.Subject) (int64, error) {
	result, err := db.Exec("update lk_subjects set subject_name=$1, updated_at=NOW(), updated_by=$2 where id=$3 RETURNING id",
		&subject.Name, &subject.UpdatedBy, &subject.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteSubject(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_subjects set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//extracuricullar
func (b JurnalRepository) GetExtracuricullars(db *sql.DB, extracuricullar models.Extracuricullar, extracuricullars []models.Extracuricullar) ([]models.Extracuricullar, error) {
	rows, err := db.Query("select * from lk_extracuricullars")

	if err != nil {
		return []models.Extracuricullar{}, err
	}

	for rows.Next() {
		err = rows.Scan(&extracuricullar.ID, &extracuricullar.Name, &extracuricullar.CreatedAt, &extracuricullar.CreatedBy, &extracuricullar.UpdatedAt, &extracuricullar.UpdatedBy, &extracuricullar.IsActive)
		extracuricullars = append(extracuricullars, extracuricullar)
	}

	if err != nil {
		return []models.Extracuricullar{}, err
	}

	return extracuricullars, nil
}

func (b JurnalRepository) GetActiveExtracuricullars(db *sql.DB, extracuricullar models.Extracuricullar, extracuricullars []models.Extracuricullar) ([]models.Extracuricullar, error) {
	rows, err := db.Query("select * from lk_extracuricullars where isactive=true")

	if err != nil {
		return []models.Extracuricullar{}, err
	}

	for rows.Next() {
		err = rows.Scan(&extracuricullar.ID, &extracuricullar.Name, &extracuricullar.CreatedAt, &extracuricullar.CreatedBy, &extracuricullar.UpdatedAt, &extracuricullar.UpdatedBy, &extracuricullar.IsActive)
		extracuricullars = append(extracuricullars, extracuricullar)
	}

	if err != nil {
		return []models.Extracuricullar{}, err
	}

	return extracuricullars, nil
}

func (b JurnalRepository) GetExtracuricullar(db *sql.DB, extracuricullar models.Extracuricullar, id string) (models.Extracuricullar, error) {
	rows := db.QueryRow("select * from lk_extracuricullars where id=$1", id)
	err := rows.Scan(&extracuricullar.ID, &extracuricullar.Name, &extracuricullar.CreatedAt, &extracuricullar.CreatedBy, &extracuricullar.UpdatedAt, &extracuricullar.UpdatedBy, &extracuricullar.IsActive)

	return extracuricullar, err
}

func (b JurnalRepository) AddExtracuricullar(db *sql.DB, extracuricullar models.Extracuricullar) (string, error) {
	err := db.QueryRow("insert into lk_extracuricullars (extracuricullar_name, created_by, updated_by, isActive) values($1, $2, $3, $4) RETURNING id;",
		extracuricullar.Name, extracuricullar.CreatedBy, extracuricullar.UpdatedBy, extracuricullar.IsActive).Scan(&extracuricullar.ID)

	if err != nil {
		return "", err
	}

	extracuricullar.IsActive = true
	return extracuricullar.ID, nil
}

func (b JurnalRepository) UpdateExtracuricullar(db *sql.DB, extracuricullar models.Extracuricullar) (int64, error) {
	result, err := db.Exec("update lk_extracuricullars set extracuricullar_name=$1, updated_at=NOW(), updated_by=$2 where id=$3 RETURNING id",
		&extracuricullar.Name, &extracuricullar.UpdatedBy, &extracuricullar.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteExtracuricullar(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_extracuricullars set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//activity
func (b JurnalRepository) GetActivities(db *sql.DB, activity models.Activity, activities []models.Activity) ([]models.Activity, error) {
	rows, err := db.Query("select * from lk_activities")

	if err != nil {
		return []models.Activity{}, err
	}

	for rows.Next() {
		err = rows.Scan(&activity.ID, &activity.Name, &activity.CreatedAt, &activity.CreatedBy, &activity.UpdatedAt, &activity.UpdatedBy, &activity.IsActive)
		activities = append(activities, activity)
	}

	if err != nil {
		return []models.Activity{}, err
	}

	return activities, nil
}

func (b JurnalRepository) GetActiveActivities(db *sql.DB, activity models.Activity, activities []models.Activity) ([]models.Activity, error) {
	rows, err := db.Query("select * from lk_activities where isactive=true")

	if err != nil {
		return []models.Activity{}, err
	}

	for rows.Next() {
		err = rows.Scan(&activity.ID, &activity.Name, &activity.CreatedAt, &activity.CreatedBy, &activity.UpdatedAt, &activity.UpdatedBy, &activity.IsActive)
		activities = append(activities, activity)
	}

	if err != nil {
		return []models.Activity{}, err
	}

	return activities, nil
}

func (b JurnalRepository) GetActivity(db *sql.DB, activity models.Activity, id string) (models.Activity, error) {
	rows := db.QueryRow("select * from lk_activities where id=$1", id)
	err := rows.Scan(&activity.ID, &activity.Name, &activity.CreatedAt, &activity.CreatedBy, &activity.UpdatedAt, &activity.UpdatedBy, &activity.IsActive)

	return activity, err
}

func (b JurnalRepository) AddActivity(db *sql.DB, activity models.Activity) (string, error) {
	err := db.QueryRow("insert into lk_activities (activity_name, created_by, updated_by, isActive) values($1, $2, $3, $4) RETURNING id;",
		activity.Name, activity.CreatedBy, activity.UpdatedBy, activity.IsActive).Scan(&activity.ID)

	if err != nil {
		return "", err
	}

	activity.IsActive = true
	return activity.ID, nil
}

func (b JurnalRepository) UpdateActivity(db *sql.DB, activity models.Activity) (int64, error) {
	result, err := db.Exec("update lk_activities set activity_name=$1, updated_at=NOW(), updated_by=$2 where id=$3 RETURNING id",
		&activity.Name, &activity.UpdatedBy, &activity.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteActivity(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_activities set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//activityDetail
func (b JurnalRepository) GetActivityDetails(db *sql.DB, activityDetail models.ActivityDetail, activityDetails []models.ActivityDetail) ([]models.ActivityDetail, error) {
	rows, err := db.Query("select * from lk_activity_details")

	if err != nil {
		return []models.ActivityDetail{}, err
	}

	for rows.Next() {
		err = rows.Scan(&activityDetail.ID, &activityDetail.Name, &activityDetail.Score, &activityDetail.ActivityID, &activityDetail.CreatedAt, &activityDetail.CreatedBy, &activityDetail.UpdatedAt, &activityDetail.UpdatedBy, &activityDetail.IsActive)
		activityDetails = append(activityDetails, activityDetail)
	}

	if err != nil {
		return []models.ActivityDetail{}, err
	}

	return activityDetails, nil
}

func (b JurnalRepository) GetActiveActivityDetails(db *sql.DB, activityDetail models.ActivityDetail, activityDetails []models.ActivityDetail) ([]models.ActivityDetail, error) {
	rows, err := db.Query("select * from lk_activity_details")

	if err != nil {
		return []models.ActivityDetail{}, err
	}

	for rows.Next() {
		err = rows.Scan(&activityDetail.ID, &activityDetail.Name, &activityDetail.Score, &activityDetail.ActivityID, &activityDetail.CreatedAt, &activityDetail.CreatedBy, &activityDetail.UpdatedAt, &activityDetail.UpdatedBy, &activityDetail.IsActive)
		activityDetails = append(activityDetails, activityDetail)
	}

	if err != nil {
		return []models.ActivityDetail{}, err
	}

	return activityDetails, nil
}

func (b JurnalRepository) GetActivityDetail(db *sql.DB, activityDetail models.ActivityDetail, id string) (models.ActivityDetail, error) {
	rows := db.QueryRow("select * from lk_activity_details where id=$1", id)
	err := rows.Scan(&activityDetail.ID, &activityDetail.Name, &activityDetail.CreatedAt, &activityDetail.CreatedBy, &activityDetail.UpdatedAt, &activityDetail.UpdatedBy, &activityDetail.IsActive)

	return activityDetail, err
}

func (b JurnalRepository) AddActivityDetail(db *sql.DB, activityDetail models.ActivityDetail) (string, error) {
	err := db.QueryRow("insert into lk_activity_details (activity_detail_name, activity_detail_score, activity_id, created_by, updated_by, isActive) values($1, $2, $3, $4, $5, $6) RETURNING id;",
		activityDetail.Name, activityDetail.Score, activityDetail.ActivityID, activityDetail.CreatedBy, activityDetail.UpdatedBy, activityDetail.IsActive).Scan(&activityDetail.ID)

	if err != nil {
		return "", err
	}

	activityDetail.IsActive = true
	return activityDetail.ID, nil
}

func (b JurnalRepository) UpdateActivityDetail(db *sql.DB, activityDetail models.ActivityDetail) (int64, error) {
	result, err := db.Exec("update lk_activity_details set activity_detail_name=$1, activity_detail_score=$2, activity_id=$3, updated_at=NOW(), updated_by=$4 where id=$5 RETURNING id",
		&activityDetail.Name, &activityDetail.Score, &activityDetail.ActivityID, &activityDetail.UpdatedBy, &activityDetail.ID)

	if err != nil {
		return 0, err
	}

	rowsUpdated, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	//area.UpdatedAt = time.Time{}
	return rowsUpdated, nil
}

func (b JurnalRepository) SoftDeleteActivityDetail(db *sql.DB, id string) (int64, error) {
	result, err := db.Exec("update lk_activity_details set updated_at= NOW(), isActive=false where id=$1", id)

	if err != nil {
		return 0, err
	}

	rowsSoftDeleted, err := result.RowsAffected()

	if err != nil {
		return 0, err
	}

	return rowsSoftDeleted, nil
}

//students
func (b JurnalRepository) GetStudents(db *sql.DB, student models.Student, students []models.Student) ([]models.Student, error) {
	rows, err := db.Query("select * from ms_students")

	if err != nil {
		return []models.Student{}, err
	}

	for rows.Next() {
		err = rows.Scan(&student.ID, &student.Code, &student.Area, &student.RegistrationDate, &student.FullName, &student.Nickname, &student.Sex, &student.Address, &student.HomePhone, &student.CellPhone, &student.LineID, &student.Email, &student.SchoolName, &student.Year, &student.FutureYear, &student.DOB, &student.Age, &student.Religion, &student.WorshipPlace, &student.IsBaptized, &student.BaptizedYear, &student.KsSiDm, &student.CreatedAt, &student.CreatedBy, &student.UpdatedAt, &student.UpdatedBy, &student.IsActive)
		students = append(students, student)
	}

	if err != nil {
		return []models.Student{}, err
	}

	return students, nil
}
