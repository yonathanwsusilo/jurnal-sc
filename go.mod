module sc-journal

go 1.13

require (
	github.com/Bowery/prompt v0.0.0-20190916142128-fa8279994f75 // indirect
	github.com/aws/aws-sdk-go v1.29.17 // indirect
	github.com/dchest/safefile v0.0.0-20151022103144-855e8d98f185 // indirect
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/kardianos/govendor v1.0.9 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.3.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/subosito/gotenv v1.2.0
	github.com/tools/godep v0.0.0-20180126220526-ce0bfadeb516 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	golang.org/x/tools v0.0.0-20200305185322-6a641547f55b // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
