package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"sc-journal/controllers"
	"sc-journal/driver"
	"sc-journal/models"

	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
)

var areas []models.Area
var db *sql.DB

func init() {
	gotenv.Load()
}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB()
	controller := controllers.Controller{}

	router := mux.NewRouter()

	//areas: localhost:8080/areas
	router.HandleFunc("/areas", controller.GetAreas(db)).Methods("GET")
	router.HandleFunc("/areas/active", controller.GetActiveAreas(db)).Methods("GET")
	router.HandleFunc("/areas/{id}", controller.GetArea(db)).Methods("GET")
	router.HandleFunc("/areas", controller.AddArea(db)).Methods("POST")
	router.HandleFunc("/areas", controller.UpdateArea(db)).Methods("PUT")
	//router.HandleFunc("/areas/{id}", controller.RemoveArea(db)).Methods("DELETE")
	router.HandleFunc("/areas/{id}", controller.SoftDeleteArea(db)).Methods("DELETE")

	//ks_si_dm: localhost:8080/ksSiDm
	router.HandleFunc("/ksSiDm", controller.GetKsSiDms(db)).Methods("GET")
	router.HandleFunc("/ksSiDm/active", controller.GetActiveKsSiDms(db)).Methods("GET")
	router.HandleFunc("/ksSiDm/{id}", controller.GetKsSiDm(db)).Methods("GET")
	router.HandleFunc("/ksSiDm", controller.AddKsSiDm(db)).Methods("POST")
	router.HandleFunc("/ksSiDm", controller.UpdateKsSiDm(db)).Methods("PUT")
	router.HandleFunc("/ksSiDm/{id}", controller.SoftDeleteKsSiDm(db)).Methods("DELETE")

	//religion: localhost:8080/religions
	router.HandleFunc("/religions", controller.GetReligions(db)).Methods("GET")
	router.HandleFunc("/religions/active", controller.GetActiveReligions(db)).Methods("GET")
	router.HandleFunc("/religions/{id}", controller.GetReligion(db)).Methods("GET")
	router.HandleFunc("/religions", controller.AddReligion(db)).Methods("POST")
	router.HandleFunc("/religions", controller.UpdateReligion(db)).Methods("PUT")
	router.HandleFunc("/religions/{id}", controller.SoftDeleteReligion(db)).Methods("DELETE")

	//subject: localhost:8080/subjects
	router.HandleFunc("/subjects", controller.GetSubjects(db)).Methods("GET")
	router.HandleFunc("/subjects/active", controller.GetActiveSubjects(db)).Methods("GET")
	router.HandleFunc("/subjects/{id}", controller.GetSubject(db)).Methods("GET")
	router.HandleFunc("/subjects", controller.AddSubject(db)).Methods("POST")
	router.HandleFunc("/subjects", controller.UpdateSubject(db)).Methods("PUT")
	router.HandleFunc("/subjects/{id}", controller.SoftDeleteSubject(db)).Methods("DELETE")

	//extracuricullar: localhost:8080/extracuricullars
	router.HandleFunc("/extracuricullars", controller.GetExtracuricullars(db)).Methods("GET")
	router.HandleFunc("/extracuricullars/active", controller.GetActiveExtracuricullars(db)).Methods("GET")
	router.HandleFunc("/extracuricullars/{id}", controller.GetExtracuricullar(db)).Methods("GET")
	router.HandleFunc("/extracuricullars", controller.AddExtracuricullar(db)).Methods("POST")
	router.HandleFunc("/extracuricullars", controller.UpdateExtracuricullar(db)).Methods("PUT")
	router.HandleFunc("/extracuricullars/{id}", controller.SoftDeleteExtracuricullar(db)).Methods("DELETE")

	//activity: localhost:8080/activities
	router.HandleFunc("/activities", controller.GetActivities(db)).Methods("GET")
	router.HandleFunc("/activities/active", controller.GetActiveActivities(db)).Methods("GET")
	router.HandleFunc("/activities/{id}", controller.GetActivity(db)).Methods("GET")
	router.HandleFunc("/activities", controller.AddActivity(db)).Methods("POST")
	router.HandleFunc("/activities", controller.UpdateActivity(db)).Methods("PUT")
	router.HandleFunc("/activities/{id}", controller.SoftDeleteActivity(db)).Methods("DELETE")

	//activityDetail: localhost:8080/activityDetails
	router.HandleFunc("/activityDetails", controller.GetActivityDetails(db)).Methods("GET")
	router.HandleFunc("/activityDetails/active", controller.GetActiveActivityDetails(db)).Methods("GET")
	router.HandleFunc("/activityDetails/{id}", controller.GetActivityDetail(db)).Methods("GET")
	router.HandleFunc("/activityDetails", controller.AddActivityDetail(db)).Methods("POST")
	router.HandleFunc("/activityDetails", controller.UpdateActivityDetail(db)).Methods("PUT")
	router.HandleFunc("/activityDetails/{id}", controller.SoftDeleteActivityDetail(db)).Methods("DELETE")

	//activityDetail: localhost:8080/userRoles
	router.HandleFunc("/userRoles", controller.GetUserRoles(db)).Methods("GET")
	router.HandleFunc("/userRoles/active", controller.GetActiveUserRoles(db)).Methods("GET")
	router.HandleFunc("/userRoles/{id}", controller.GetUserRole(db)).Methods("GET")
	router.HandleFunc("/userRoles", controller.AddUserRole(db)).Methods("POST")
	router.HandleFunc("/userRoles", controller.UpdateUserRole(db)).Methods("PUT")
	router.HandleFunc("/userRoles/{id}", controller.SoftDeleteUserRole(db)).Methods("DELETE")

	//users: localhost:8080/users
	router.HandleFunc("/users", controller.GetUsers(db)).Methods("GET")

	router.HandleFunc("/students", controller.GetStudents(db)).Methods("GET")

	fmt.Print("Server is running at port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
