package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var areas []models.Area

func logFatalArea(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetAreas(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var area models.Area
		var error models.Error

		areas = []models.Area{}
		areaRepo := jurnalRepository.JurnalRepository{}

		areas, err := areaRepo.GetAreas(db, area, areas)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, areas)
	}
}

func (c Controller) GetActiveAreas(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var area models.Area
		var error models.Error

		areas = []models.Area{}
		areaRepo := jurnalRepository.JurnalRepository{}

		areas, err := areaRepo.GetActiveAreas(db, area, areas)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, areas)
	}
}

func (c Controller) GetArea(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var area models.Area
		var error models.Error

		params := mux.Vars(r)

		areas = []models.Area{}
		areaRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		area, err := areaRepo.GetArea(db, area, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, area)
	}
}

func (c Controller) AddArea(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var area models.Area
		var areaID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&area)

		if area.Name == "" || area.Code == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		area.IsActive = true

		areaRepo := jurnalRepository.JurnalRepository{}
		areaID, err := areaRepo.AddArea(db, area)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		area.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, areaID)
	}
}

func (c Controller) UpdateArea(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var area models.Area
		var error models.Error

		json.NewDecoder(r.Body).Decode(&area)

		if area.ID == "" || area.Name == "" || area.Code == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		areaRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := areaRepo.UpdateArea(db, area)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) RemoveArea(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		areaRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsDeleted, err := areaRepo.RemoveArea(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsDeleted)
	}
}

func (c Controller) SoftDeleteArea(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		areaRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := areaRepo.SoftDeleteArea(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
