package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var subjects []models.Subject

func logFatalSubject(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetSubjects(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var subject models.Subject
		var error models.Error

		subjects = []models.Subject{}
		subjectRepo := jurnalRepository.JurnalRepository{}

		subjects, err := subjectRepo.GetSubjects(db, subject, subjects)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, subjects)
	}
}

func (c Controller) GetActiveSubjects(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var subject models.Subject
		var error models.Error

		subjects = []models.Subject{}
		subjectRepo := jurnalRepository.JurnalRepository{}

		subjects, err := subjectRepo.GetActiveSubjects(db, subject, subjects)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, subjects)
	}
}

func (c Controller) GetSubject(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var subject models.Subject
		var error models.Error

		params := mux.Vars(r)

		subjects = []models.Subject{}
		subjectRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		subject, err := subjectRepo.GetSubject(db, subject, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, subject)
	}
}

func (c Controller) AddSubject(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var subject models.Subject
		var subjectID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&subject)

		if subject.Name == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		subject.IsActive = true

		subjectRepo := jurnalRepository.JurnalRepository{}
		subjectID, err := subjectRepo.AddSubject(db, subject)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		subject.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, subjectID)
	}
}

func (c Controller) UpdateSubject(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var subject models.Subject
		var error models.Error

		json.NewDecoder(r.Body).Decode(&subject)

		if subject.ID == "" || subject.Name == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		subjectRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := subjectRepo.UpdateSubject(db, subject)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteSubject(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		subjectRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := subjectRepo.SoftDeleteSubject(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
