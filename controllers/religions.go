package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var religions []models.Religion

func logFatalReligion(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetReligions(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var religion models.Religion
		var error models.Error

		religions = []models.Religion{}
		religionRepo := jurnalRepository.JurnalRepository{}

		religions, err := religionRepo.GetReligions(db, religion, religions)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, religions)
	}
}

func (c Controller) GetActiveReligions(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var religion models.Religion
		var error models.Error

		religions = []models.Religion{}
		religionRepo := jurnalRepository.JurnalRepository{}

		religions, err := religionRepo.GetActiveReligions(db, religion, religions)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, religions)
	}
}

func (c Controller) GetReligion(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var religion models.Religion
		var error models.Error

		params := mux.Vars(r)

		religions = []models.Religion{}
		religionRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		religion, err := religionRepo.GetReligion(db, religion, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, religion)
	}
}

func (c Controller) AddReligion(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var religion models.Religion
		var religionID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&religion)

		if religion.Name == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		religion.IsActive = true

		religionRepo := jurnalRepository.JurnalRepository{}
		religionID, err := religionRepo.AddReligion(db, religion)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		religion.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, religionID)
	}
}

func (c Controller) UpdateReligion(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var religion models.Religion
		var error models.Error

		json.NewDecoder(r.Body).Decode(&religion)

		if religion.ID == "" || religion.Name == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		religionRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := religionRepo.UpdateReligion(db, religion)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteReligion(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		religionRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := religionRepo.SoftDeleteReligion(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
