package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

var ksSiDms []models.KsSiDm

func logFatalKsSiDm(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetKsSiDms(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ksSiDm models.KsSiDm
		var error models.Error

		ksSiDms = []models.KsSiDm{}
		ksSiDmRepo := jurnalRepository.JurnalRepository{}

		ksSiDms, err := ksSiDmRepo.GetKsSiDms(db, ksSiDm, ksSiDms)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, ksSiDms)
	}
}

func (c Controller) GetActiveKsSiDms(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ksSiDm models.KsSiDm
		var error models.Error

		ksSiDms = []models.KsSiDm{}
		ksSiDmRepo := jurnalRepository.JurnalRepository{}

		ksSiDms, err := ksSiDmRepo.GetActiveKsSiDms(db, ksSiDm, ksSiDms)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, ksSiDms)
	}
}

func (c Controller) GetKsSiDm(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ksSiDm models.KsSiDm
		var error models.Error

		params := mux.Vars(r)

		ksSiDms = []models.KsSiDm{}
		ksSiDmRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		ksSiDm, err := ksSiDmRepo.GetKsSiDm(db, ksSiDm, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, ksSiDm)
	}
}

func (c Controller) AddKsSiDm(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ksSiDm models.KsSiDm
		var ksSiDmID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&ksSiDm)

		if ksSiDm.Name == "" || ksSiDm.Code == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		ksSiDm.IsActive = true

		ksSiDmRepo := jurnalRepository.JurnalRepository{}
		ksSiDmID, err := ksSiDmRepo.AddKsSiDm(db, ksSiDm)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		ksSiDm.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, ksSiDmID)
	}
}

func (c Controller) UpdateKsSiDm(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ksSiDm models.KsSiDm
		var error models.Error

		json.NewDecoder(r.Body).Decode(&ksSiDm)

		if ksSiDm.ID == "" || ksSiDm.Name == "" || ksSiDm.Code == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		ksSiDmRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := ksSiDmRepo.UpdateKsSiDm(db, ksSiDm)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteKsSiDm(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		ksSiDmRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := ksSiDmRepo.SoftDeleteKsSiDm(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
