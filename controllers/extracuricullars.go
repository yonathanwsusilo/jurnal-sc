package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var extracuricullars []models.Extracuricullar

func logFatalExtracuricullar(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetExtracuricullars(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var extracuricullar models.Extracuricullar
		var error models.Error

		extracuricullars = []models.Extracuricullar{}
		extracuricullarRepo := jurnalRepository.JurnalRepository{}

		extracuricullars, err := extracuricullarRepo.GetExtracuricullars(db, extracuricullar, extracuricullars)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, extracuricullars)
	}
}

func (c Controller) GetActiveExtracuricullars(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var extracuricullar models.Extracuricullar
		var error models.Error

		extracuricullars = []models.Extracuricullar{}
		extracuricullarRepo := jurnalRepository.JurnalRepository{}

		extracuricullars, err := extracuricullarRepo.GetActiveExtracuricullars(db, extracuricullar, extracuricullars)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, extracuricullars)
	}
}

func (c Controller) GetExtracuricullar(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var extracuricullar models.Extracuricullar
		var error models.Error

		params := mux.Vars(r)

		extracuricullars = []models.Extracuricullar{}
		extracuricullarRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		extracuricullar, err := extracuricullarRepo.GetExtracuricullar(db, extracuricullar, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, extracuricullar)
	}
}

func (c Controller) AddExtracuricullar(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var extracuricullar models.Extracuricullar
		var extracuricullarID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&extracuricullar)

		if extracuricullar.Name == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		extracuricullar.IsActive = true

		extracuricullarRepo := jurnalRepository.JurnalRepository{}
		extracuricullarID, err := extracuricullarRepo.AddExtracuricullar(db, extracuricullar)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		extracuricullar.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, extracuricullarID)
	}
}

func (c Controller) UpdateExtracuricullar(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var extracuricullar models.Extracuricullar
		var error models.Error

		json.NewDecoder(r.Body).Decode(&extracuricullar)

		if extracuricullar.ID == "" || extracuricullar.Name == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		extracuricullarRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := extracuricullarRepo.UpdateExtracuricullar(db, extracuricullar)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteExtracuricullar(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		extracuricullarRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := extracuricullarRepo.SoftDeleteExtracuricullar(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
