package controllers

import (
	"database/sql"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"
)

var students []models.Student

func logFatalStudent(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (c Controller) GetStudents(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var student models.Student
		var error models.Error

		students = []models.Student{}
		studentRepo := jurnalRepository.JurnalRepository{}

		students, err := studentRepo.GetStudents(db, student, students)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, students)
	}
}
