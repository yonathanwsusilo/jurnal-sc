package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	jurnalRepository "sc-journal/repository/jurnal"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var activities []models.Activity
var activityDetails []models.ActivityDetail

func logFatalActivity(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

//activity
func (c Controller) GetActivities(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activity models.Activity
		var error models.Error

		activities = []models.Activity{}
		activityRepo := jurnalRepository.JurnalRepository{}

		activities, err := activityRepo.GetActivities(db, activity, activities)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activities)
	}
}

func (c Controller) GetActiveActivities(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activity models.Activity
		var error models.Error

		activities = []models.Activity{}
		activityRepo := jurnalRepository.JurnalRepository{}

		activities, err := activityRepo.GetActiveActivities(db, activity, activities)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activities)
	}
}

func (c Controller) GetActivity(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activity models.Activity
		var error models.Error

		params := mux.Vars(r)

		activities = []models.Activity{}
		activityRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		activity, err := activityRepo.GetActivity(db, activity, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activity)
	}
}

func (c Controller) AddActivity(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activity models.Activity
		var activityID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&activity)

		if activity.Name == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		activity.IsActive = true

		activityRepo := jurnalRepository.JurnalRepository{}
		activityID, err := activityRepo.AddActivity(db, activity)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		activity.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, activityID)
	}
}

func (c Controller) UpdateActivity(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activity models.Activity
		var error models.Error

		json.NewDecoder(r.Body).Decode(&activity)

		if activity.ID == "" || activity.Name == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		activityRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := activityRepo.UpdateActivity(db, activity)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteActivity(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		activityRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := activityRepo.SoftDeleteActivity(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}

//activityDetail
func (c Controller) GetActivityDetails(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activityDetail models.ActivityDetail
		var error models.Error

		activityDetails = []models.ActivityDetail{}
		activityDetailRepo := jurnalRepository.JurnalRepository{}

		activityDetails, err := activityDetailRepo.GetActivityDetails(db, activityDetail, activityDetails)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activityDetails)
	}
}

func (c Controller) GetActiveActivityDetails(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activityDetail models.ActivityDetail
		var error models.Error

		activityDetails = []models.ActivityDetail{}
		activityDetailRepo := jurnalRepository.JurnalRepository{}

		activityDetails, err := activityDetailRepo.GetActiveActivityDetails(db, activityDetail, activityDetails)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activityDetails)
	}
}

func (c Controller) GetActivityDetail(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activityDetail models.ActivityDetail
		var error models.Error

		params := mux.Vars(r)

		activityDetails = []models.ActivityDetail{}
		activityDetailRepo := jurnalRepository.JurnalRepository{}

		id := params["id"]

		activityDetail, err := activityDetailRepo.GetActivityDetail(db, activityDetail, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, activityDetail)
	}
}

func (c Controller) AddActivityDetail(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activityDetail models.ActivityDetail
		var activityDetailID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&activityDetail)

		if activityDetail.Name == "" || activityDetail.Score == 0 || activityDetail.ActivityID == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		activityDetail.IsActive = true

		activityDetailRepo := jurnalRepository.JurnalRepository{}
		activityDetailID, err := activityDetailRepo.AddActivityDetail(db, activityDetail)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		activityDetail.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, activityDetailID)
	}
}

func (c Controller) UpdateActivityDetail(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var activityDetail models.ActivityDetail
		var error models.Error

		json.NewDecoder(r.Body).Decode(&activityDetail)

		if activityDetail.ID == "" || activityDetail.Name == "" || activityDetail.Score == 0 || activityDetail.ActivityID == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		activityDetailRepo := jurnalRepository.JurnalRepository{}
		rowsUpdated, err := activityDetailRepo.UpdateActivityDetail(db, activityDetail)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteActivityDetail(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		activityDetailRepo := jurnalRepository.JurnalRepository{}
		id := params["id"]

		rowsSoftDeleted, err := activityDetailRepo.SoftDeleteActivityDetail(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}
