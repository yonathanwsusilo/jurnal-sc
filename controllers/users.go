package controllers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"sc-journal/models"
	userRepository "sc-journal/repository/user"
	"sc-journal/utils"

	"github.com/gorilla/mux"
)

//type Controller struct{}

var userRoles []models.UserRole
var users []models.User

func logFatalUser(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

//userRoles
func (c Controller) GetUserRoles(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole models.UserRole
		var error models.Error

		userRoles = []models.UserRole{}
		userRoleRepo := userRepository.UserRepository{}

		userRoles, err := userRoleRepo.GetUserRoles(db, userRole, userRoles)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, userRoles)
	}
}

func (c Controller) GetActiveUserRoles(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole models.UserRole
		var error models.Error

		userRoles = []models.UserRole{}
		userRoleRepo := userRepository.UserRepository{}

		userRoles, err := userRoleRepo.GetActiveUserRoles(db, userRole, userRoles)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, userRoles)
	}
}

func (c Controller) GetUserRole(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole models.UserRole
		var error models.Error

		params := mux.Vars(r)

		userRoles = []models.UserRole{}
		userRoleRepo := userRepository.UserRepository{}

		id := params["id"]

		userRole, err := userRoleRepo.GetUserRole(db, userRole, id)

		if err != nil {
			if err == sql.ErrNoRows {
				error.Message = "Not Found"
				utils.SendError(w, http.StatusNotFound, error)
				return
			} else {
				error.Message = "Server error"
				utils.SendError(w, http.StatusInternalServerError, error)
				return
			}
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, userRole)
	}
}

func (c Controller) AddUserRole(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole models.UserRole
		var userRoleID string
		var error models.Error

		json.NewDecoder(r.Body).Decode(&userRole)

		if userRole.Name == "" {
			error.Message = "Enter missing fields."
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		userRole.IsActive = true

		userRoleRepo := userRepository.UserRepository{}
		userRoleID, err := userRoleRepo.AddUserRole(db, userRole)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		userRole.IsActive = true

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, userRoleID)
	}
}

func (c Controller) UpdateUserRole(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole models.UserRole
		var error models.Error

		json.NewDecoder(r.Body).Decode(&userRole)

		if userRole.ID == "" || userRole.Name == "" {
			error.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, error)
			return
		}

		//area.UpdatedAt = time.Now()

		userRoleRepo := userRepository.UserRepository{}
		rowsUpdated, err := userRoleRepo.UpdateUserRole(db, userRole)

		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		//area.UpdatedAt =

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsUpdated)
	}
}

func (c Controller) SoftDeleteUserRole(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var error models.Error
		params := mux.Vars(r)
		userRoleRepo := userRepository.UserRepository{}
		id := params["id"]

		rowsSoftDeleted, err := userRoleRepo.SoftDeleteUserRole(db, id)

		if err != nil {
			error.Message = "Server error."
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		if rowsSoftDeleted == 0 {
			error.Message = "Not Found"
			utils.SendError(w, http.StatusNotFound, error)
			return
		}

		w.Header().Set("Content-Type", "text/plain")
		utils.SendSuccess(w, rowsSoftDeleted)
	}
}

//users
func (c Controller) GetUsers(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user models.User
		var error models.Error

		users = []models.User{}
		userRepo := userRepository.UserRepository{}

		users, err := userRepo.GetUsers(db, user, users)
		if err != nil {
			error.Message = "Server error"
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		utils.SendSuccess(w, users)
	}
}
